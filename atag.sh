#!/bin/bash

# delete tags. As precaution, always pre-set to next build when pushed.
#git tag --delete REL_44
#git tag --delete COMP_6.6
#git tag --delete DMAP_6.6
#git tag --delete PRIM_6.6
#git tag --delete DEM_1.5

# Suite release
git tag  REL_44

# EFIS apps
git tag  COMP_6.6
git tag  DMAP_6.6
git tag  PRIM_6.6

# DEM data pacs
#git tag  DEM_1.4

#git push --tags origin
#git push --tags origin 
#git push --tags origin 
#git push --tags origin 


# delete remote tags - careful, very careful!
#git tag --delete REL_40_
#git tag --delete COMP_6.2_
#git tag --delete DMAP_6.2_
#git tag --delete PRIM_6.2_
#git push --delete origin REL_40_
#git push --delete origin COMP_6.2_
#git push --delete origin DMAP_6.2_
#git push --delete origin PRIM_6.2_




