#!/bin/bash

pskill java
#pskill adb

rm ./apk/kwik-efis.apk
rm ./apk/kwik-dmap.apk
rm ./apk/kwik-comp.apk

if [ "$1" == "all" ]
then 
  # Default to the full settings.gradle 
  echo "include ':pfd', ':mfd', ':cfd', ':ulib', ':data', ':datapac'" > ./settings.gradle
  rm ./apk/kwik-efis-datapac-zar.aus.apk
  rm ./apk/kwik-efis-datapac-usa.can.apk
  rm ./apk/kwik-efis-datapac-eur.rus.apk
  rm ./apk/kwik-efis-datapac-sah.jap.apk
  rm ./apk/kwik-efis-datapac-pan.arg.apk
  rm ./apk/kwik-efis-datapac-ant.spl.apk
else 
  # Default to abbreviated settings.gradle 
  echo "include ':pfd', ':mfd', ':cfd', ':ulib'" > ./settings.gradle
fi

cat ./settings.gradle
./gradlew clean
#./gradlew build
#./gradlew assemble
./gradlew assembleDebug

cp ./pfd/build/outputs/apk/debug/pfd-debug.apk ./apk/kwik-efis.apk
cp ./mfd/build/outputs/apk/debug/mfd-debug.apk ./apk/kwik-dmap.apk
cp ./cfd/build/outputs/apk/debug/cfd-debug.apk ./apk/kwik-comp.apk
if [ "$1" == "all" ]
then 
  cp ./datapac/build/outputs/apk/eur_rus/debug/datapac-eur_rus-debug.apk ./apk/kwik-efis-datapac-eur.rus.apk
  cp ./datapac/build/outputs/apk/pan_arg/debug/datapac-pan_arg-debug.apk ./apk/kwik-efis-datapac-pan.arg.apk
  cp ./datapac/build/outputs/apk/sah_jap/debug/datapac-sah_jap-debug.apk ./apk/kwik-efis-datapac-sah.jap.apk
  cp ./datapac/build/outputs/apk/usa_can/debug/datapac-usa_can-debug.apk ./apk/kwik-efis-datapac-usa.can.apk
  cp ./datapac/build/outputs/apk/zar_aus/debug/datapac-zar_aus-debug.apk ./apk/kwik-efis-datapac-zar.aus.apk
  cp ./datapac/build/outputs/apk/ant_spl/debug/datapac-ant_spl-debug.apk ./apk/kwik-efis-datapac-ant.spl.apk
fi
cp ./CHANGELOG.md ./apk/CHANGELOG.md

pskill java
#exit

cp ./pfd/build/outputs/apk/debug/pfd-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis.apk
cp ./mfd/build/outputs/apk/debug/mfd-debug.apk v:/Sync/Public/KwikEFIS/kwik-dmap.apk
cp ./cfd/build/outputs/apk/debug/cfd-debug.apk v:/Sync/Public/KwikEFIS/kwik-comp.apk
if [ "$1" == "all" ]
then 
  cp ./datapac/build/outputs/apk/eur_rus/debug/datapac-eur_rus-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-eur.rus.apk
  cp ./datapac/build/outputs/apk/pan_arg/debug/datapac-pan_arg-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-pan.arg.apk
  cp ./datapac/build/outputs/apk/sah_jap/debug/datapac-sah_jap-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-sah.jap.apk
  cp ./datapac/build/outputs/apk/usa_can/debug/datapac-usa_can-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-usa.can.apk
  cp ./datapac/build/outputs/apk/zar_aus/debug/datapac-zar_aus-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-zar.aus.apk
  cp ./datapac/build/outputs/apk/zar_aus/debug/datapac-ant_spl-debug.apk v:/Sync/Public/KwikEFIS/kwik-efis-datapac-ant.spl.apk
fi
cp ./CHANGELOG.md v:/Sync/Public/KwikEFIS/CHANGELOG.md

pskill java

# Restore the settings.gradle to a full build
echo "include ':pfd', ':mfd', ':cfd', ':ulib', ':data', ':datapac'" > ./settings.gradle
exit


