/*
 * Copyright (C) 2016 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import android.content.Context;
import android.util.Log;
import player.ulib.UNavigation;

public class Gpx
{
	private static Context context = null;
	public static ArrayList<Apt> aptList = null;
    public static boolean bReady = false;

    public static void setContext(Context ctx)
    {
        context = ctx;
        aptList = new ArrayList<Apt>();
    }

    public static String unAccent(String s) {
        //
        // JDK1.5
        //   use sun.text.Normalizer.normalize(s, Normalizer.DECOMP, 0);
        //
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public static void loadDatabase(float lat, float lon)
    {
        bReady = false;
        if ((context != null) && (aptList != null)) {
            XmlPullParserFactory pullParserFactory;
            try {
                pullParserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = pullParserFactory.newPullParser();

                InputStream in_s = context.getAssets().open("waypoint/airport.gpx.xml");
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in_s, null);
                parseXML(parser);
            }
            catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            bReady = true;

            final int KEEP_MAX_WPTS = 1000; // Maximum number of nearest wpts to keep
            float range = UNavigation.EARTH_CIRC_2; //  Start with half the earth
            while (aptList.size() > KEEP_MAX_WPTS) {
                for (int i = aptList.size() - 1; i > 0; i--) {
                    Apt currApt = aptList.get(i);
                    if ((UNavigation.calcDme(lat, lon, currApt.lat, currApt.lon) > range)
                            && (currApt.fixed == false)) {
                        aptList.remove(i);
                    }
                }
                range *= 0.922; // 85% of surface = sqrt(0.85) // UTrig.M_SQRT1_2;  // sqrt(0.5) for half the surface
            }
            Log.v("kwik", "range: " + (int) range + " airports: " + aptList.size());
        }
        else {
            throw new NullPointerException();
        }
    }

    private static void parseXML(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        int eventType = parser.getEventType();
        Apt currentWpt = null;
        int ctr = 0;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String txt = null;
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    // To help avoid the ConcurrentModificationException
                    aptList.clear();
                    break;

                case XmlPullParser.START_TAG:
                    txt = parser.getName();
                    if (txt.equals("wpt")) {
                        currentWpt = new Apt();
                        if (parser.getAttributeCount() == 2) {
                            currentWpt.lat = Float.valueOf(parser.getAttributeValue(0));
                            currentWpt.lon = Float.valueOf(parser.getAttributeValue(1));
                        }
                    }
                    else if (currentWpt != null) {
                        if (txt.equals("ele")) {
                            currentWpt.elev = Float.valueOf(parser.nextText());
                        }
                        if (txt.equals("name")) {
                            currentWpt.name = parser.nextText();
                        }
                        else if (txt.equals("cmt")) {
                            currentWpt.cmt = unAccent(parser.nextText());
                        }
                        else if (txt.equals("fixed")) {
                            currentWpt.fixed = Boolean.valueOf(parser.nextText());
                        }
                    }
                    break;

                case XmlPullParser.END_TAG:
                    txt = parser.getName();
                    // Only add non null wpt's that contain exactly 4 upper-case numbers or letters
                    if (txt.equalsIgnoreCase("wpt")
                            && currentWpt != null
                            && currentWpt.name.length() == 4
                            && currentWpt.name.matches("[0-Z]+")) {
                        aptList.add(currentWpt);
                        ctr++;
                    }
            }
            eventType = parser.next();
        }
        Log.v("kwik", "parsed: " + ctr + " airports: " + aptList.size());
        //printProducts(aptList); // only used for debugging
    }

	public static ArrayList<Apt> getAptSelect(float lat, float lon, int range, int nr) 
	{
		ArrayList<Apt> nearestAptList = new ArrayList<Apt>();

		Iterator <Apt> it = aptList.iterator(); 
		while (it.hasNext())
		{
			Apt currProduct  = it.next();
			
			// add code to determine  the <nr> apts in range 
			double deltaLat = lat - currProduct.lat;
			double deltaLon = lon - currProduct.lon;
			// double d =  Math.hypot(deltaLon, deltaLat);  // in degree, 1 deg = 60 nm 
			double d =  Math.sqrt(deltaLon*deltaLon + deltaLat*deltaLat);  // faster then hypot, see www 
			
			if (d < range) {
				nearestAptList.add(currProduct);
			}
		}
		return nearestAptList;
	}

	private void printProducts(ArrayList<Apt> list)
	{
		String content = "";
		Iterator <Apt> it = list.iterator(); 
		while (it.hasNext())  
		{
			Apt currProduct  = it.next();
			content = content + "\nName :" +  currProduct.name + "\n";
			content = content + "Cmt :" +  currProduct.cmt + "\n";
			System.out.println(content);
            Log.v("kwik", "b2 - " + content);
		}
		//Log.v("b2", "b2 - " + content);
		//TextView display = (TextView)findViewById(R.id.info);
		//display.setText(content);
	}
}

