/*
 * Copyright (C) 2017 Player One
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package player.efis.common;

// Standard imports
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.*;
import java.math.BigInteger;
import java.util.concurrent.Semaphore;

import player.ulib.UMath;


/*

a
 +------------------------------------+
 |            b                       |     a = demTopLeftLat,  demTopLeftLon
 |             +---------+            |     b = x0, y0
 |             |         |            |     c = lat0, lon0
 |             |  c +    |            |
 |             |         |            |
 |             +---------+            |
 |                                    |
 |             |< BUFX  >|            |
 |                                    |
 +------------------------------------+

 Note: BUFX should fit completely in the DEM tile
       Consider this when choosing size
*/

public class OpenWeatherMap
{
    Context context;
    public static Bitmap mBitmap;  // the actual weather map.

    public static int zoomOWM;// = 0;  // 360 x 180
    public static float SPANLON; //= 360f / (float) Math.pow(2, zoomOWM);
    public static float SPANLAT; // = 180f / (float) Math.pow(2, zoomOWM);

    //static final int MAX_ELEV = 6000;  // in meters

    public static  int BUFX;// = 341; // 256; // for some reason the loaded bitmap is a different size?
    public static  int BUFY;// = 341; //256;

    //static DemColor colorTbl[] = new DemColor[MAX_ELEV];  // 600*3 = r*3

    static final float demTopLeftLat = +90;
    static final float demTopLeftLon = -180;

    static public float lat0;    // center of the BUFX tile
    static public float lon0;    // center of the BUFX tile

    //public static float gamma = 1;

    public static float gps_lon;
    public static float gps_lat;

    public static int bmTL_x;
    public static int bmTL_y;

    public static final String checksum = "f31cba1b06b4f63ac8ff5c298afac78e";

    private static Semaphore mutex = new Semaphore(1, true);

    //-------------------------------------------------------------------------
    // Construct a new default loader with no flags set
    //
    public OpenWeatherMap()
    {
    }

    public OpenWeatherMap(Context context)
    {
        this.context = context;
        //setGamma(1);
    }

    public static String decode(String s)
    {
        StringBuilder buff = new StringBuilder(s);
        StringBuilder mask = new StringBuilder(s);

        for (char i = 0; i < 32; i++) {
            char c = Integer.toString(15 - i % 16, 16).charAt(0);
            mask.setCharAt(i, c);
            buff.setCharAt(i, '0');
        }
        BigInteger a = new BigInteger(s, 16);
        BigInteger b = new BigInteger(mask.toString(), 16);

        String r = a.xor(b).toString(16);
        return (buff.toString() + r).substring(r.length()) ;
    }


    //-------------------------------------------------------------------------
    // Geographic coordinates (lat, lon)
    // in decimal degrees
    //
    public static void setLatLon(float lat, float lon)
    {
        gps_lat = lat;
        gps_lon = lon;
    }


    public static void setBitmap(Bitmap bm)
    {
        try {
            mutex.acquire();

            mBitmap = bm;

            if (mBitmap != null) {
                BUFX = mBitmap.getWidth();
                BUFY = mBitmap.getHeight();
            }
            else {
                BUFX = 0;
                BUFY = 0;

                SPANLON = 0;
                SPANLAT = 0;
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            mutex.release();
        }
    }

    public static void setzoomOWM(int z)
    {
        zoomOWM = z;

        SPANLON = 360f / (float) Math.pow(2, z);  // 22.5 for 4
        SPANLAT = 180f / (float) Math.pow(2, z);  // 11.25 for 4

        bmTL_x = UMath.trunc((180.0f + gps_lon) / SPANLON);
        bmTL_y = UMath.trunc((90.0f  - gps_lat) / SPANLAT);

        //demTopLeftLon = -180 + + bmTL_x * SPANLON;
        //demTopLeftLat = 90 - (bmTL_y) * SPANLAT;

        float clon = (-180 + bmTL_x * SPANLON) + SPANLON / 2;
        float clat = (90 - bmTL_y * SPANLAT) - SPANLAT / 2;

        setBufferCenter(clat, clon);  // wx_200: 67,5, -135
        setDEMRegionTile(clat, clon);  //?? remove todo
    }


    /*public static void setGamma(float g)
    {
        gamma = g;

        //for (short i = 0; i < colorTbl.length; i++) colorTbl[i] = calcColor(i);
        for (short i = 0; i < colorTbl.length; i++) colorTbl[i] = calcHSVColor(i); //optimal so far!
    }*/


    static public int getDbz(float lat, float lon)
    {
        int dbz;

        if (mBitmap == null)
            return 0;

        // Note this is also where the 2x difference between lat and lon is handled

        int x =          BUFX/2 + UMath.trunc((lon - lon0) * BUFX / SPANLON);
        //int y =  BUFY/2 - UMath.trunc((lat - lat0) * BUFY / SPANLAT)/2  +  BUFY/4; // right for Darwin YPDN and WPFL - debug version
        int y =  BUFY - (BUFY/2 + UMath.trunc((lat - lat0) * BUFY / SPANLAT)) / 2; // same as above, /2 handles the 2x difference

        if ((x < 0) || (y < 0) || (x >= BUFX) || (y >= BUFY))
            return 0;
        else dbz = mBitmap.getPixel(x, y);

        return dbz;
    }

    /*private static DemColor calcHSVColor(short c)
    {

        int r = 600;  // 600m=2000ft
        int MaxColor = 128;
        float hsv[] = {0, 0, 0};
        int colorBase;
        int min_v = 25;

        int v = MaxColor * c / r;

        if (v > 3 * MaxColor) {
            // mountain
            v %= MaxColor;
            colorBase = Color.rgb(MaxColor - v, MaxColor, MaxColor);
        }
        else if (v > 2 * MaxColor) {
            // highveld
            v %= MaxColor;
            colorBase = Color.rgb(MaxColor, MaxColor, v); // keep building to white
        }
        else if (v > 1 * MaxColor) {
            // inland
            v %= MaxColor;
            colorBase = Color.rgb(v, MaxColor, 0);
        }
        else if (v > 0) {
            // coastal plain
            if (v > min_v)
                colorBase = Color.rgb(0, v, 0);
            else {
                // close to the sea (lower than 25m),
                // is a special case otherwise it gets too dark
                colorBase = Color.rgb(min_v - v, min_v + (min_v - v), min_v - v);
            }
        }
        else {
            // the ocean
            colorBase = Color.rgb(0, 0, MaxColor); //bright blue ocean
        }

        // this allows us to adjust hue, sat and val
        Color.colorToHSV(colorBase, hsv);
        hsv[0] = hsv[0];  // hue 0..360
        hsv[1] = hsv[1];  // sat 0..1
        hsv[2] = hsv[2];  // val 0..1

        if (hsv[2] > 0.25) {
            hsv[0] = hsv[0] - ((hsv[2] - 0.25f) * 60);  // adjust the hue max 15%,  hue 0..360
            hsv[2] = 0.30f; // clamp the value, val 0..1
        }


        //On HSV model, H (hue) define the base color, S (saturation) control the amount of gray
        //and V controls the brightness. So, if you enhance V and decrease S at same time, you gets
        //more luminance

        hsv[1] = hsv[1]/gamma;  // sat 0..1
        hsv[2] = hsv[2]*gamma;  // val 0..1

        int color = Color.HSVToColor(hsv);
        return new DemColor((float) Color.red(color) / 255, (float) Color.green(color) / 255, (float) Color.blue(color) / 255);
    }*/
    //-----------------------------


    /*public void setBufferCenter(float lat, float lon)
    {
        lat0 = lat;
        lon0 = lon;
        x0 = (int) (Math.abs(lon0 - demTopLeftLon) * 60 * 2) - BUFX / 2;  // *60 min  * 2 pix/min (aka 30 sec)
        y0 = (int) (Math.abs(lat0 - demTopLeftLat) * 60 * 2) - BUFY / 2;
    }*/

    public static void setBufferCenter(float lat, float lon)
    {
        lat0 = lat;
        lon0 = lon;

        // x0 and y0 are in pixels
        int x0 = UMath.trunc(Math.abs(lon0 - demTopLeftLon) * BUFX / SPANLON) - BUFX / 2;   // 1 xwxpixel = BUFX / SPANLON
        int y0 = UMath.trunc(Math.abs(lat0 - demTopLeftLat) * BUFY / SPANLAT) - BUFY / 2;   // 1 ywxpixel = BUFY / SPANLAT
    }

    // Note - we can refactor out demTopLeftLon and Lat later.


    //-------------------------------------------------------------------------
    // use the lat lon to determine which tile is active
    //
    public static String setDEMRegionTile(float lat, float lon)
    {
        //demTopLeftLat = +90;  // todo calculate
        //demTopLeftLon = -180; // todo calculate

        String sTile = String.format("%c%03d%c%02d",
                demTopLeftLon < 0 ? 'W' : 'E', (int) Math.abs(demTopLeftLon),
                demTopLeftLat < 0 ? 'S' : 'N', (int) Math.abs(demTopLeftLat));

        return sTile;
    }





}


/*
Tiles:
Zoom 0 = 0,0      360  x 180
Zoom 1 = 0 -> 1   180  x 90 deg
Zoom 2 = 0 -> 3   90   x 45 deg
Zoom 3 = 0 -> 7   45   x 22.5 deg
Zoom 4 = 0 -> 15  22.5 x 11.25 deg
Zoom 5 = 0 -> 31  11.25 x 5.625 deg

Zoom 10 = 0 -> 1023
Bitmap is 256 x 256
https://tile.openweathermap.org/map/precipitation_new/4/15/7.png?appid=0dc0008370e0c42a3623e6b1fcaef59e
*/
