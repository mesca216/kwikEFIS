package player.efis.common;

public class OpenAirspacePoint
{
    public float lat;
    public float lon;

    public OpenAirspacePoint(float lat, float lon)
    {
        this.lat = lat;
        this.lon = lon;
    }
}
