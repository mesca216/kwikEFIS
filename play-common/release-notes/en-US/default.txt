REL_44 (2021-04-14)
------------------
* Add Antarctica DEM 
* Add Antarctica support to DemGTOPO30 (ant-npl is non-standard DEM)
* Add special handling near the geographic poles
* Gpx database infinite loop bugfix
* Release EFIS v6.6
* Release DMAP v6.6
* Release COMP v6.6
